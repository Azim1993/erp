<?php

namespace App\Http\Controllers\HR\Employee;

use App\HR\Employee\Attendence;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendenceController extends Controller
{
    public function store(Request $request)
    {
        Attendence::create([
            'user_id' => $request->user_id,
            'date'    => Carbon::now()->toDateString(),
            'in_time'    => Carbon::now()->toTimeString(),
            ]);
    }

    public function left(AttendenceRequest $request)
    {
        $employeePresent = $this->selectPresent(1);
        $employeeIn = Carbon::parse($employeePresent->in_time);
        $employeeOut = Carbon::now()->toTimeString();
        $employeeOutParse = Carbon::parse($employeeOut);
        $totalTime = $employeeOutParse->diffInMinutes($employeeIn);

        $employeePresent->update([
            'out_time' => $employeeOut,
            'total_time' => $totalTime
            ]);        
    }

    private function selectPresent($user_id)
    {
        return Attendence::where([
                'date'      => Carbon::now()->toDateString(),
                'user_id'   => $user_id
            ])->first();
    }
}
