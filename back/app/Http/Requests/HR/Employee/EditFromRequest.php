<?php

namespace App\Http\Requests\HR\Employee;

use Illuminate\Foundation\Http\FormRequest;

class EditFromRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required | max:100',
            'last_name' => 'required | max:100',
            'email' => 'required | email | unique:employee_personals,email,' . $this->id,
            'phone' => 'required | max:50 | unique:employee_personals,phone,' . $this->id,
            'dob' => 'required | date',
            'address' => 'required',
            'sub_district_id' => 'required | numeric',
            'district_id' => 'required | numeric',
            'division_id' => 'required | numeric',
            'nid' => 'required | max:17 | unique:employee_personals,nid,' . $this->id,
            'zip_code' => 'required',
            'country' => 'required | max:100',
        ];
    }
}
