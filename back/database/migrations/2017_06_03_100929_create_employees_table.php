<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 60);
            $table->string('last_name', 60);
            $table->string('email',150);
            $table->string('phone',50);
            $table->timestamp('dob')->nullable();
            $table->text('address');
            $table->integer('sub_district_id')->unsigned();
            $table->string('nid', 20);
            $table->string('zip_code', 10)->nullable();
            $table->string('country', 60)->default('Bangladesh');
            $table->timestamps(); 
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
