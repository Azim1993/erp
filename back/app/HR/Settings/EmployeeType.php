<?php

namespace App\HR\Settings;

use Illuminate\Database\Eloquent\Model;

class EmployeeType extends Model
{
    protected $fillable = [
    	'name','description','slug','status'
    ];

    public function setNameAttribute($value)
    {
    	$this->attributes['name'] = $value;
    	$this->attributes['slug'] = str_slug( $value, $separator = '_');
    }
    
    public function administrative()
    {
        return $this->hasMany(HR\Employee\Administrative::class);
    }
}
