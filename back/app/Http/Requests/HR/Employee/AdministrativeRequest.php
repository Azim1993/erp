<?php

namespace App\Http\Requests\HR\Employee;

use Illuminate\Foundation\Http\FormRequest;

class AdministrativeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'user_id'        => 'required|integer', 
           'branch_id'      => 'required|integer', 
           'department_id'      => 'required|integer', 
           'designation'    => 'required|string|max:100', 
           'joining_date'   => 'required', 
           'employee_type'  => 'required', 
           'working_days'   => 'required|string', 
           'working_hour'   => 'required|string', 
           'salary'         => 'required', 
           'monthly_leave'  => 'required'
        ];
    }
}
