import user from './Auth'
export default {
  guest (to, from, next) {
    next(!user.check() ? true : {
      path: '/',
      query: {
        redirect: to.name
      }
    })
  },
  auth (to, from, next) {
    next(user.check() ? true : {
      path: '/login',
      query: {
        redirect: to.name
      }
    })
  }
}
