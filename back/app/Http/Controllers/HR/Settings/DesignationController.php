<?php

namespace App\Http\Controllers\HR\Settings;

use App\HR\Settings\Designation;
use App\Http\Controllers\Controller;
use App\Http\Requests\HR\Settings\DesignationRequest;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designation = Designation::latest()->get();
        return response()->json([
            'designation' => $designation
            ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DesignationRequest $request)
    {
        $store = Designation::create($request->all());
        return response()->json([
            'designation' => $store
            ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HR\Settings\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function show(Designation $designation)
    {
        return response()->json([
            'designation' => $designation
            ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HR\Settings\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function edit(Designation $designation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HR\Settings\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function update(DesignationRequest $request, Designation $designation)
    {
        $designation->update($request->all());
        return response()->json([
            'designation' => $designation
            ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HR\Settings\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Designation $designation)
    {
        $designation = $designation->delete();
        return response()->json([
            'designation' => $designation
            ], 200);
    }
}
