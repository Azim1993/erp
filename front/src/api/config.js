export const baseUrl = 'http://erp.dev'
export const tokenName = 'ErpAuthUserToken'
export let token = window.localStorage.getItem(tokenName)
export function authHeader (tokenData = token) {
  if (tokenData === null) {
    tokenData = window.localStorage.getItem(tokenName)
  }
  return tokenData
}
export function setToken (value) {
  token = value
  return token
}
export const loginHeader = {
  headers: {
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
  }
}

export function header () {
  console.log(authHeader())
  return {
    'Accept': 'application/json',
    Authorization: 'Bearer' + authHeader(),
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/x-www-form-urlencoded'
  }
}
export const getHeader = function () {
  const tokenData = window.localStorage.getItem('ErpAuthUserToken')
  const headers = {
    'Accept': 'application/json',
    'Authorization': 'Bearer' + tokenData,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/x-www-form-urlencoded'
  }
  return headers
}
