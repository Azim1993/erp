<?php

namespace App\HR\Employee;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
	use SoftDeletes;

	protected $dates = ['dob', 'deleted_at'];

	protected $fillable = [
    	'first_name', 'last_name', 'email','phone', 'gender','address',
    	'sub_district_id', 'country','nid','zip_code','dob'
    ];

    public function setDobAttribute($value)
    {
        $this->attributes['dob'] = Carbon::parse($value);
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
