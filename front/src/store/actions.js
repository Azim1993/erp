export default {
  login (token) {
    return new Promise(resolve => {
      setTimeout(() => {
        localStorage.setItem('token', token)
        resolve()
      }, 1000)
    })
  },
  logout () {
    localStorage.removeItem('token')
  }
}
