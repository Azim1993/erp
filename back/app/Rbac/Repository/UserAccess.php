<?php
namespace App\Rbac\Repository;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserAccess {

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function check()
    {
        foreach ($this->permissions() as $value) {
            if( config("permissions.{$value->permission_id}.route") == $this->getRouteKey())
            return true;
        }
        return false;
    }

    public function permissions()
    {
        $user = $this->request->user()->load('permissions');
        return $user->permissions;
    }

    public function user()
    {
        return JWTAuth::parseToken()->authenticate();
    }

    public function getRouteKey()
    {
        return $this->request->route()->getName();
    }
}
