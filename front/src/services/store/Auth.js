export default function (Vue) {
  Vue.auth = {
    setToken (token) {
      return localStorage.setItem('api_token', token)
    },
    getToken () {
      let token = localStorage.getItem('api_token')
      return token !== undefined ? token : null
    },
    destroyToken () {
      return localStorage.removeItem('api_token')
    },
    isAuthenticated () {
      return this.getToken() === true
    }
  }

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get: () => {
        return Vue.auth
      }
    }
  })
}
