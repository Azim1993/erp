<?php

namespace App\Rbac;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function permissions()
    {
    	return $this->belongsToMany(App\Rbac\Permission::class, 'roles_permissions');
    }
}
