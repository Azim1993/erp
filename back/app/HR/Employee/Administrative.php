<?php

namespace App\HR\Employee;

use Illuminate\Database\Eloquent\Model;

class Administrative extends Model
{
    protected $fillable = [
    	'user_id','branch_id','designation_id','joining_date',
    	'employee_type_id','working_days','working_hour','salary',
    	'monthly_leave','department_id','joinLetter','ini_password'
    ];

    protected $hidden = ['ini_password'];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function branch()
    {
        return $this->belongsTo(\App\HR\Settings\Branch::class);
    }

    public function department()
    {
        return $this->belongsTo(\App\HR\Settings\Department::class);
    }

    public function designation()
    {
        return $this->belongsTo(\App\HR\Settings\Designation::class);
    }

    public function employeeType()
    {
        return $this->belongsTo(\App\HR\Settings\EmployeeType::class);
    }
}
