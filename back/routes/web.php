<?php
Auth::loginUsingId(1);
Route::get('/', function() {
    $lists = Route::getRoutes();
    foreach ($lists as $list) {
        $name[] = $list->getName();
    }

    return $name;
})->name('home.route');
Route::get('/group', function() {
    foreach(config('permissionGroup') as $group)
    {
        // $g[] = $group['name'];
        foreach($group['permissions'] as $permission)
        {
            $g[$group['name']][] = config('permissions.'.$permission);
        }
    }
    return response()->json($g);
});
