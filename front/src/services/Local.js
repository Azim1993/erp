export const datePickerLocal = {
  dow: 4,
  hourTip: 'Select Hour',
  minuteTip: 'Select Minute',
  secondTip: 'Select Second',
  yearSuffix: '',
  monthsHead: 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
  months: 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
  weeks: 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_')
}
