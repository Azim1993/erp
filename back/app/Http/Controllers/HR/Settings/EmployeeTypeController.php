<?php

namespace App\Http\Controllers\HR\Settings;

use App\HR\Settings\EmployeeType;
use App\Http\Controllers\Controller;
use App\Http\Requests\HR\Settings\EmployeeTypeRequest;

class EmployeeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeTypes = EmployeeType::latest()->get();
        return response()->json([
            'employeeTypes' => $employeeTypes
            ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeTypeRequest $request)
    {
        $employeeType = EmployeeType::create($request->all());
        return response()->json([
            'employeeType' => $employeeType
            ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HR\Settings\EmployeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeType $employeeType)
    {
        return $employeeType;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HR\Settings\EmployeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeType $employeeType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HR\Settings\EmployeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeTypeRequest $request, EmployeeType $employeeType)
    {
        $employeeType->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HR\Settings\EmployeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeType $employeeType)
    {
        $employeeType->delete();
    }
}
