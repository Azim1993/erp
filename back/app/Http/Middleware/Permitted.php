<?php

namespace App\Http\Middleware;

use App\Rbac\Repository\UserAccess;
use Closure;

class Permitted
{
    private $access;

    public function __construct(UserAccess $access)
    {
        $this->access = $access;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->access->check())
        {
            return response('forbidden', 403);;
        }
        return $next($request);
    }
}
