<?php

namespace App\Http\Controllers\HR\Expense;

use App\HR\Expense\Expense;
use App\Http\Controllers\Controller;
use App\Http\Requests\HR\Employee\ExpenseFormRequest;
// use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseFormRequest $request)
    {
        Expense::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employees\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        return $expense;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employees\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit(Expense $expense)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employees\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseFormRequest $request, Expense $expense)
    {
        $expense->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employees\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        $expense->delete();
    }
}
