<?php

namespace App\Http\Controllers\HR\Employee;

use App\HR\Employee\Administrative;
use App\Http\Controllers\Controller;
use App\Http\Requests\HR\Employee\AdministrativeRequest;
// use Illuminate\Http\Request;

class AdministrativeController extends Controller
{
    public static function storeWithArrange($user, $administrative, $finalInfo, $iniPassword)
    {
        if($administrative && $user && $finalInfo)
        {
            $administrativeArrange['branch_id'] = $administrative['branch']['id'];
            $administrativeArrange['department_id'] = $administrative['department']['id'];
            $administrativeArrange['designation_id'] = $administrative['designation']['id'];
            $administrativeArrange['salary'] = $administrative['salary'];
            $administrativeArrange['employee_type_id'] = $administrative['type']['id'];
            $administrativeArrange['working_days'] = $administrative['workingDays'];
            $administrativeArrange['working_days_type'] = $administrative['workingDaysType'];
            $administrativeArrange['working_hour'] = ($administrative['workingTime']['HH']*60)+$administrative['workingTime']['mm'];
            $administrativeArrange['monthly_leave'] = $administrative['workingLeaves'];
            $administrativeArrange['joining_date'] = $finalInfo['joinDate'];
            $administrativeArrange['joinLetter'] = $finalInfo['letter'];
            $administrativeArrange['ini_password'] = $iniPassword;
            $store = $user->administrative()->create($administrativeArrange);

            if ($store) return true;
        }
        return;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = Administrative::all();
        return $info;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdministrativeRequest $request)
    {
        $store = Administrative::create($request->all());
        return $store;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employees\Administrative  $administrative
     * @return \Illuminate\Http\Response
     */
    public function show(Administrative $administrative)
    {
        return $administrative;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employees\Administrative  $administrative
     * @return \Illuminate\Http\Response
     */
    public function edit(Administrative $administrative)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employees\Administrative  $administrative
     * @return \Illuminate\Http\Response
     */
    public function update(AdministrativeRequest $request, Administrative $administrative)
    {
         $administrative->update($request->all());

        return response()->json([
            'data' => [
                'success' => 'Info updated successfully'
            ]
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employees\Administrative  $administrative
     * @return \Illuminate\Http\Response
     */
    public function destroy(Administrative $administrative)
    {
        $administrative->delete();

        return response()->json([
            'data' => [
                'success' => 'Deleted successfully'
            ]
        ], 200);
    }
}
