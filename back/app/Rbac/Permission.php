<?php

namespace App\Rbac;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function roles()
    {
    	return $this->belongsToMany(\App\Rbac\Role::class, 'roles_permissions');
    }
}
