import {tokenName} from '@/api/config'
export default {
  user () {
    return !!localStorage.getItem(tokenName)
  },
  check () {
    return !!localStorage.getItem(tokenName)
  }
}
