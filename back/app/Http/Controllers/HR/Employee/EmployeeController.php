<?php

namespace App\Http\Controllers\HR\Employee;

use App\Http\Controllers\Auth\PermissionController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HR\Employee\AdministrativeController;
use App\Http\Requests\HR\Employee\{StoreFormRequest, UpdateFormRequest};
use App\Providers\StrongPassword;
use App\Transformers\HR\Employee\EmployeeTransformer;
use App\User;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Tymon\JWTAuth\Facades\JWTAuth;

class EmployeeController extends Controller
{
    use StrongPassword;
	/**
	 * Display all resources
	 * 
	 * @return [type] [description]
	 */
	public function index()
	{

        $employees = User::status()
                        ->include()
                        ->orderBy('created_at', 'desc')
                        ->paginate(20);
        // return response($employees);
        return fractal()
            ->collection($employees)
            ->transformWith(new EmployeeTransformer)
            // ->serializeWith(new JsonApiSerializer())
            ->paginateWith(new IlluminatePaginatorAdapter($employees))
            ->toArray();
	}

    /**
     * Show specific Employee Resource
     * 
     * @param  Employee $employee [description]
     * @return [type]             [description]
     */
    public function show(User $employee)
    {
        return fractal()
            ->item($employee)
            ->transformWith(new EmployeeTransformer)
            ->toArray();
    }

	/**
	 * Store Employee Informations
	 * 
	 * @param  StoreFormRequest $request [description]
	 * @return [type]                    [description]
	 */
    public function store(StoreFormRequest $request)
    {
        $password =  $this->generator(15);
        
        // user table
        $employee = $this->createUser($request->id, $password);

        // // permission assign
        PermissionController::store($employee, $request->administrative['permission']);

        // personal info store
        $employee->employee()->create($request->personal);
        
        // administrative info 
        AdministrativeController::storeWithArrange($employee,$request->administrative, $request->finalInfo, $password);

    	return fractal()
            ->item($employee)
            ->transformWith(new EmployeeTransformer)
            ->toArray();
    }

    private function createUser($id, $password)
    {
        $userId = ['identity_key' => $id];
        $hashPassword =  ['password' => bcrypt($password)];
        $user = array_merge($userId, $hashPassword);
        $employee = User::create($user);
        return $employee;
    }

    /**
	 * Update Employee Informations
	 * 
	 * @param  UpdateFormRequest $request [description]
	 * @return [type]                    [description]
	 */
    public function update(UpdateFormRequest $request, Employee $employee)
    {
    	$employee->update($request->all());

    	return response()->json([
    		'data' => [
                'success' => 'Updated Successfully'
            ]
    	], 200);
    }

    /**
     * SoftDeletes Employee informations
     * 
     * @param  Employee $employee [description]
     * @return [type]             [description]
     */
    public function destroy(Employee $employee)
    {
    	$employee->delete();

    	return response()->json([
    		'success' => 'Profile of `' . $employee->fullName() . '` deleted successfully'
    	], 200);
    }
}
