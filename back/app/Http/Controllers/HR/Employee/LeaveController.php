<?php

namespace App\Http\Controllers\HR\Employee;

use App\HR\Employee\ApprovalAuthority;
use App\HR\Employee\Leave;
use App\Http\Controllers\Controller;
use App\Http\Requests\HR\Employee\EmployeeLeaveRequest;
// use Illuminate\Http\Request;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allLeaves = Leave::all();
        return $allLeaves;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeLeaveRequest $request)
    {
        $leave = Leave::create($request->all());

        if(is_array($request->input('tagAuthors')))
        {
            foreach($request->input('tagAuthors') as $authority)
            {
                $ss[] = $leave->tagsTo()->create([
                    'user_id' => $request->user_id,
                    'authority_id' => $authority
                    ]);
            }
        }else{
            $leave->tagsTo()->create([
                'user_id' => $request->input('user_id'),
                'authority_id' => $request->input('tagAuthors')
                ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employees\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function show(Leave $leaf)
    {
        return $leaf;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employees\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function edit(Leave $leaf)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employees\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeLeaveRequest $request, Leave $leaf)
    {
        $update = $leaf->update($request->all());
        if($update) return $update;
        return ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employees\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function destroy(Leave $leaf)
    {
        $destroy = $leaf->delete();
        $leaf->tagsTo()->delete();
        if($destroy) return 'Success';
        return 'failed';
    }

    public function countLeave($id)
    {
        $leave = Leave::where('user_id',$id)->first();
        dd($leave->diff_date());
        $c =Leave::get();
        foreach ($c as $value) {
            $p[] = $value->diff_date();
        }dd($p);        
    }
}
