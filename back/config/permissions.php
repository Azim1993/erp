<?php
return [
        0 => [
            'id' =>0,
            'route' => 'employee.index',
            'description' => 'all employee'
        ],
        1 => [
            'id' =>1,
            'route' => 'employees.store',
            'description' => 'store employee'
        ],
        2 => [
            'id' =>2,
            'route' => 'employee.show',
            'description' => 'show employee'
        ],
        3 => [
            'id' =>3,
            'route' => 'employees.update',
            'description' => 'update employee'
        ],
        4 => [
            'id' =>4,
            'route' => 'employee.destroy',
            'description' => 'delete employee'
        ],
        5 => [
            'id' =>5,
            'route' => 'administrative.index',
            'description' => 'all employee'
        ],
        6 => [
            'id' =>6,
            'route' => 'administrative.store',
            'description' => 'store administrative'
        ],
        7 => [
            'id' =>7,
            'route' => 'administrative.show',
            'description' => 'show administrative'
        ],
        8 => [
            'id' =>8,
            'route' => 'administrative.update',
            'description' => 'update administrative'
        ],
        9 => [
            'id' =>9,
            'route' => 'administrative.destroy',
            'description' => 'delete administrative'
        ],
        10 => [
            'id' =>10,
            'route' => 'presents.index',
            'description' => 'all presents'
        ],
        11 => [
            'id' =>11,
            'route' => 'presents.store',
            'description' => 'store presents'
        ],
        12 => [
            'id' =>12,
            'route' => 'presents.show',
            'description' => 'show  presents'
        ],
        13 => [
            'id' =>13,
            'route' => 'presents.update',
            'description' => 'update  presents'
        ],
        14 => [
            'id' =>14,
            'route' => 'presents.destroy',
            'description' => 'delete presents'
        ],
        15 => [
            'id' =>15,
            'route' => 'employee.left',
            'description' => 'left time'
        ],
        16 => [
            'id' =>16,
            'route' => 'leaves.index',
            'description' => 'all leaves'
        ],
        17 => [
            'id' =>17,
            'route' => 'leaves.store',
            'description' => 'store leaves'
        ],
        18 => [
            'id' =>18,
            'route' => 'leaves.show',
            'description' => 'show  leaves'
        ],
        19 => [
            'id' =>19,
            'route' => 'leaves.update',
            'description' => 'update  leaves'
        ],
        20 => [
            'id' =>20,
            'route' => 'leaves.destroy',
            'description' => 'delete leaves'
        ],
        21 => [
            'id' =>21,
            'route' => 'employee.leaves.count',
            'description' => 'leaves count'
        ],
        22 => [
            'id' =>22,
            'route' => 'branches.index',
            'description' => 'all branches'
        ],
        23 => [
            'id' =>23,
            'route' => 'branches.store',
            'description' => 'store branches'
        ],
        24 => [
            'id' =>24,
            'route' => 'branches.show',
            'description' => 'show  branches'
        ],
        25 => [
            'id' =>25,
            'route' => 'branches.update',
            'description' => 'update  branches'
        ],
        26 => [
            'id' =>26,
            'route' => 'branches.destroy',
            'description' => 'delete branches'
        ],
        27 => [
            'id' =>27,
            'route' => 'departments.index',
            'description' => 'all departments'
        ],
        28 => [
            'id' =>28,
            'route' => 'departments.store',
            'description' => 'store departments'
        ],
        29 => [
            'id' =>29,
            'route' => 'departments.show',
            'description' => 'show  departments'
        ],
        30 => [
            'id' =>30,
            'route' => 'departments.update',
            'description' => 'update  departments'
        ],
        31 => [
            'id' =>31,
            'route' => 'departments.destroy',
            'description' => 'delete departments'
        ],
        32 => [
            'id' =>32,
            'route' => 'designation.index',
            'description' => 'all designation'
        ],
        33 => [
            'id' =>33,
            'route' => 'designation.store',
            'description' => 'store designation'
        ],
        34 => [
            'id' =>34,
            'route' => 'designation.show',
            'description' => 'show  designation'
        ],
        35 => [
            'id' =>35,
            'route' => 'designation.update',
            'description' => 'update  designation'
        ],
        36 => [
            'id' =>36,
            'route' => 'designation.destroy',
            'description' => 'delete designation'
        ],
        37 => [
            'id' =>37,
            'route' => 'employeeTypes.index',
            'description' => 'all employeeTypes'
        ],
        38 => [
            'id' =>38,
            'route' => 'employeeTypes.store',
            'description' => 'store employeeTypes'
        ],
        39 => [
            'id' =>39,
            'route' => 'employeeTypes.show',
            'description' => 'show  employeeTypes'
        ],
        40 => [
            'id' =>40,
            'route' => 'employeeTypes.update',
            'description' => 'update  employeeTypes'
        ],
        41 => [
            'id' =>41,
            'route' => 'employeeTypes.destroy',
            'description' => 'delete employeeTypes'
        ],
        42 => [
            'id' =>42,
            'route' => 'expense.index',
            'description' => 'all expense'
        ],
        43 => [
            'id' =>43,
            'route' => 'expense.store',
            'description' => 'store expense'
        ],
        44 => [
            'id' =>44,
            'route' => 'expense.show',
            'description' => 'show  expense'
        ],
        45 => [
            'id' =>45,
            'route' => 'expense.update',
            'description' => 'update  expense'
        ],
        46 => [
            'id' =>46,
            'route' => 'expense.destroy',
            'description' => 'delete expense'
        ]
    ];
?>