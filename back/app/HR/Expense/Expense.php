<?php

namespace App\HR\Employee;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [
    	'start_date' ,'end_date', 'user_id','approval_authority',
    	'amount','description','user_note','admin_note'
    ];

    public function tagsTo()
    {
        return $this->morphMany('App\ApprovalAuthority', 'tagable');
    }
}
