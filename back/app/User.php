<?php

namespace App;

use App\Rbac\HasPermissionsTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identity_key', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function permissions()
    {
        return $this->hasMany(Rbac\UserPermission::class);
    }
    public function employee() 
    {
        return $this->hasOne(HR\Employee\Employee::class);
    }
    public function administrative() 
    {
        return $this->hasOne(HR\Employee\Administrative::class);
    }
    public function scopeStatus($query)
    {
        return $query->where('status', 1);
    }
    public function scopeInclude($query)
    {
        return $query->with('employee','administrative');
    }

}
