<?php

namespace App\HR\Employee;

use Illuminate\Database\Eloquent\Model;

class Attendence extends Model
{
    protected $fillable = [
    	'user_id','date','in_time','out_time','total_time'
    ];
}
