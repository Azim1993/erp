<?php

namespace App\HR\Employee;

use App\HR\Employee\ApprovalAuthority;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $fillable = [
    	'user_id', 'start_date', 'end_date', 'note', 'status', 'approved'
    ];

    // protected $attr = ['diff_date'];

    public function tagsTo()
    {
        return $this->morphMany(ApprovalAuthority::class, 'tagable');
    }

	public function diff_date()
    {
        return Carbon::parse($this->start_date)->diffIndays(Carbon::parse($this->end_date));
    }

    public function approved()
    {
    	return $this->approved == true;
    }
}
