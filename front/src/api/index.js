import axios from 'axios'
import { baseUrl, authHeader } from './config'
axios.defaults.baseURL = baseUrl
axios.defaults.headers.common['Authorization'] = 'Bearer' + authHeader()
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
export default {
  request (method, uri, data = null) {
    if (!method) {
      console.error('API function call requires method argument')
      return
    }

    if (!uri) {
      console.error('API function call requires uri argument')
      return
    }

    let url = uri
    return new Promise(function (resolve, reject) {
      axios({ method, url, data })
      .then(function (res) {
        resolve(res)
      })
      .catch(function (error) {
        if (error.response.data.error === 'token_expired' || error.response.data.error === 'token_invalid') {
          window.localStorage.removeItem('ErpAuthUserToken')
        }
        reject(error.response.data)
      })
    })
  }
}
