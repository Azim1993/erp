import Vue from 'vue'
import Router from 'vue-router'
import Guard from '@/services/Middleware'
import Home from '@/components/Home'
import Employees from '@/components/hrm/employee/show/all/Employees'
import EmployeeCreate from '@/components/hrm/employee/create/Create'
import EmployeesProfile from '@/components/hrm/employee/show/profile/Index'
import EmployeeExpence from '@/components/hrm/expence/Index'
import Employeeleave from '@/components/hrm/leaves/Index'
import Settings from '@/components/hrm/settings/Index'
import Login from '@/components/auth/Login'
import notFound from '@/components/handel/404'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      beforeEnter: Guard.auth
    },
    {
      path: '/employee/create',
      name: 'EmployeeCreate',
      component: EmployeeCreate,
      beforeEnter: Guard.auth
    },
    {
      path: '/employee/all',
      name: 'Employees',
      component: Employees,
      beforeEnter: Guard.auth
    },
    {
      path: '/employee/profile/:userId',
      name: 'EmployeeProfile',
      component: EmployeesProfile,
      beforeEnter: Guard.auth
    },
    {
      path: '/employee/expence',
      name: 'EmployeeExpence',
      component: EmployeeExpence,
      beforeEnter: Guard.auth
    },
    {
      path: '/employee/leaves',
      name: 'Employeeleave',
      component: Employeeleave,
      beforeEnter: Guard.auth
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
      beforeEnter: Guard.auth
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: Guard.guest,
      meta: { fullView: true }
    },
    {
      path: '*',
      name: 'NotFound',
      component: notFound,
      meta: { fullView: true }
    }
  ]
})
