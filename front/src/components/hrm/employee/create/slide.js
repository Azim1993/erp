// Using a basic set of effect
var slideEffects = {
  enter: function (el, insert, timeout) {
    console.log('enter')
    var duration = 0.35
    el.style.maxHeight = el.scrollHeight + 'px'
    insert()
    TweenMax.to(el, duration, {
      ease: Power4.easeOut,
      maxHeight: el.scrollHeight
    })
  },
  leave: function (el, remove, timeout) {
    console.log('leave')
    var duration = 0.35
    el.style.maxHeight = el.scrollHeight + 'px'
    TweenMax.to(el, duration, {
      maxHeight: '0',
      padding: '0',
      ease: Power4.easeOut,
      onComplete: remove
    })
  }
}
