<?php

namespace App\HR\Employee;

use Illuminate\Database\Eloquent\Model;

class ApprovalAuthority extends Model
{
    protected $fillable = [
    	'note','user_id', 'authority_id', 'tagable_id', 'tagable_type','status'
    ];
    public function authority()
    {
    	return $this->hasOne(App\User::class,'id','authority_id');
    }
    public function user()
    {
    	return $this->hasOne(App\User::class);
    }
    public function tagable()
    {
        return $this->morphTo();
    }
}
