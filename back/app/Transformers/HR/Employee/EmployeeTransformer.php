<?php 

namespace App\Transformers\HR\Employee;

use App\User;
use League\Fractal\TransformerAbstract;

class EmployeeTransformer extends TransformerAbstract
{
	public function transform(User $employee)
	{
		return [
			'id'          	=> $employee->id,
			'identity_key' 	=> $employee->identity_key,
			'status' 		=> $employee->status,
			'created_at'  	=> $employee->created_at->format('M d, Y'),
			'created_at_human'  => $employee->created_at->diffForHumans(),
			'first_name'  	=> $employee->employee->first_name,
			'last_name'   	=> $employee->employee->last_name,
			'full_name'   	=> $employee->employee->fullName(),
			'email'       	=> $employee->employee->email, 
			'phone'       	=> $employee->employee->phone, 
			'dob'         	=> $employee->employee->dob->format('M d, Y'),
			'age'         	=> $employee->employee->dob->age,
			'gender'        => $employee->employee->gender,
			'address'     	=> $employee->employee->address, 
			'nid'         	=> $employee->employee->nid, 
			'country'     	=> $employee->employee->country,
			'zip_code'     	=> $employee->employee->zip_code,
			'branch' 		=> $employee->administrative->branch,
			'department' 	=> $employee->administrative->department,
			'designation' 	=> $employee->administrative->designation,
			'employee_type' => $employee->administrative->employeeType,
			'joinLetter' 	=> $employee->administrative->joinLetter,
			'joining_date' 	=> $employee->administrative->joining_date,
			'monthly_leave' => $employee->administrative->monthly_leave,
			'salary' 		=> $employee->administrative->salary,
			'working_days' 	=> $employee->administrative->working_days,
			'working_hour' 	=> $employee->administrative->working_hour,
		];
	}
}