<?php

namespace App\Http\Controllers\HR\Settings;

use App\HR\Settings\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests\HR\Settings\DepartmentFormRequest;
// use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = Department::latest()->get();
        return response()->json([
                'departments' => $department
            ], 200); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentFormRequest $request)
    {
        $department = Department::create($request->all());
        return response()->json([
                'departments' => $department
            ], 200);     
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employees\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        return $department;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employees\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employees\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentFormRequest $request, Department $department)
    {
        $department->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employees\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();
    }
}
