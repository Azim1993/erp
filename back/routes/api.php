<?php
Route::post('/login', 'Auth\AuthController@login')->name('auth.login');
Route::group([], function() {
	Route::post('/logout', 'Auth\AuthController@logout')->name('auth.logout');
	Route::group(['prefix' => 'HRM', 'namespace' => 'HR'], function() {
		Route::resource('/employees', 'Employee\EmployeeController', ['except' => ['create', 'edit']]);
		Route::resource('/presents', 'Employee\AttendenceController',['except'=> ['edit','create']]);
		Route::resource('/administrative', 'Employee\AdministrativeController',['except'=> ['edit','create']]);
		Route::get('/left','Employee\AttendenceController@left')->name('employee.left');
		Route::resource('/leaves', 'Employee\LeaveController',['except'=> ['edit','create']]);
		Route::get('/count_leaves/{id}', 'Employee\LeaveController@countLeave')->name('employee.leaves.count');
		Route::resource('/branches', 'Settings\BranchController',['except'=> ['edit','create']]);
		Route::resource('/departments', 'Settings\DepartmentController',['except'=> ['edit','create']]);
		Route::resource('/designation', 'Settings\DesignationController',['except'=> ['edit','create']]);
		Route::resource('/employeeTypes', 'Settings\EmployeeTypeController',['except'=> ['edit','create']]);
		Route::resource('/expense', 'Expense\ExpenseController',['except'=> ['edit','create']]);
	});
});
Route::get('/group', 'Auth\PermissionController@permissions')->name('permission.group');
Route::get('/salary', 'HR\Employee\SalaryController@index');
