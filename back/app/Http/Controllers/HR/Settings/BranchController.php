<?php

namespace App\Http\Controllers\HR\Settings;

use App\HR\Settings\Branch;
use App\Http\Controllers\Controller;
use App\Http\Requests\HR\Settings\BranchRequest;
// use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::all();
        return response()->json([
            'data' => [
                'branches' => $branches,
                'success' => 'Branch info created successfully'
            ]
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchRequest $request)
    {
        $branch = Branch::create($request->all());

        return response()->json([
            'branch' => $branch
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employees\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        return $branch;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employees\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employees\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(BranchRequest $request, Branch $branch)
    {
        $branch->update($request->all());

        return response()->json([
            'data' => [
                'success' => 'Branch info updated successfully'
            ]
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employees\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();

        return response()->json([
            'data' => [
                'success' => 'Branch info deleted successfully'
            ]
        ], 200);
    }
}
