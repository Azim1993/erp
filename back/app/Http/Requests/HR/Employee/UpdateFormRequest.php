<?php

namespace App\Http\Requests\HR\Employee;

use App\Http\Requests\FormRequest;

class UpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        
        return [
            'first_name' => 'required|string|min:3|max:100',
            'last_name' => 'required|string|min:3|max:100',
            'email' => 'required|email|unique:employees,email,' . $id,
            'phone' => 'required|max:50|unique:employees,phone,' . $id,
            'dob' => 'required|date',
            'address' => 'required',
            'sub_district_id' => 'required|numeric',
            'nid' => 'required|max:17|unique:employees,nid,' . $id,
            'zip_code' => 'required|string',
            'country' => 'required|string|max:100'
        ];
    }
}
