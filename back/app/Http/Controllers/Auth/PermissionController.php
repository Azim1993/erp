<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    protected function permissions()
    {
        $permissionGroup = [];
        foreach(config('permissionGroup') as $group)
        {
            $permissions = [];
            foreach($group['permissions'] as $key => $permission)
            {
                $permissions[$key] = config('permissions.'.$permission);
            }
            $permissionGroup[] = array(
                'group' => $group['name'],
                'active' => false,
                'permissions' => $permissions
            );
        }
        return response()->json($permissionGroup);
    }

    public static function store($user, $permissions)
    {
        if(is_array($permissions) && !empty($permissions))
        {
            foreach ($permissions as $permission) {
                $store = $user->permissions()->create([
                    'permission_id' => $permission
                ]);
            }
            if ($store) return true;
        }
        return;
    }
}
