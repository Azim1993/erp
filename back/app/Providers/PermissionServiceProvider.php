<?php

namespace App\Providers;

use App\Permission;
use Gate;
// use Illuminate\Auth\Access\Gate;
// use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Permission::get()->map( function($permission) {
        //     Gate::define($permission->name, function($user) use ($permission) {
        //         return $user->hasPermissionTo($permission);
        //     });
        // });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
