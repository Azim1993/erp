<?php
return [
    [
        'name' =>'employee',
        'permissions' => [0,1,2,3,4]
    ],
    [
        'name' =>'administrative',
        'permissions' => [5,6,7,8,9]
    ],
    [
        'name' =>'presents',
        'permissions' => [10,11,12,13,14]
    ],
    [
        'name' =>'leaves',
        'permissions' => [16,17,18,19,20,21]
    ],
    [
        'name' =>'branches',
        'permissions' => [22,23,24,25,26]
    ],
    [
        'name' =>'departments',
        'permissions' => [27,28,29,30,31]
    ],
    [
        'name' =>'designation',
        'permissions' => [32,33,34,35,36]
    ],
    [
        'name' =>'employeeTypes',
        'permissions' => [37,38,39,40,41]
    ],
    [
        'name' =>'expense',
        'permissions' => [42,43,44,45,46]
    ]
]
?>