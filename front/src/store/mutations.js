export default {
  setToken (state, token) {
    state.token = token
    localStorage.setItem('token', token)
    state.token = localStorage.getItem('token')
  },
  clearToken (state) {
    state.token = null
    localStorage.removeItem('token')
  }
}

