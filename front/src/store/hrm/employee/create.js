export default {
  state: {
    newEmployee: {
      personal: [],
      administrative: [],
      id: '',
      finalInfo: []
    }
  },
  getters: {
    getEmployee (state) {
      return state.newEmployee
    },
    getPersonal (state) {
      return state.newEmployee.personal
    },
    getAdministrative (state) {
      return state.newEmployee.administrative
    },
    getId (state) {
      return state.newEmployee.id
    },
    getFinalInfo (state) {
      return state.newEmployee.finalInfo
    }
  },
  mutations: {
    setPersonal (state, payload) {
      state.newEmployee.personal = payload
    },
    setAdministrative (state, payload) {
      state.newEmployee.administrative = payload
    },
    setId (state, payload) {
      state.newEmployee.id = payload
    },
    setFinalInfo (state, payload) {
      state.newEmployee.finalInfo = payload
    }
  }
}
