// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import Notifications from 'vue-notification'
import VeeValidate from 'vee-validate'
import VModal from 'vue-js-modal'
import '../node_modules/semantic-ui-css/semantic.min.css'
import Trend from 'vuetrend'

Vue.config.productionTip = false

Vue.use(Trend)
Vue.use(Notifications)
Vue.use(VeeValidate)
Vue.use(VModal)

export const eventBusForEmployeeCreate = new Vue({
  methods: {
    changeComponentChild (eventComponent) {
      this.$emit('changeComponentParent', eventComponent)
    }
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
