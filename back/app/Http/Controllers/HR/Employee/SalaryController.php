<?php

namespace App\Http\Controllers\HR\Employee;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SalaryController extends Controller
{
    protected function index()
    {
        // $a_date = date('Y-m-d');
        // $lastDateOfMonth = date("t", strtotime($a_date));
        $now = Carbon::now();
        $monthLength = $now->daysInMonth;
        $value = 1;
        $count = 1;
        $day = '';
        $holyDays = ['friday', 'saturday'];
        while ($value <= $monthLength) {
            $day = Carbon::create($now->format('Y'), $now->format('m'), $value)->format('l');
            if (!in_array(strtolower($day), $holyDays))
            {
                $monthDate[] = $day; 
                $workingDays = $count++;
            }
            $value++;
        }

        return response($workingDays);
    }
}
