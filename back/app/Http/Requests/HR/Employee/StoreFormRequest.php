<?php

namespace App\Http\Requests\HR\Employee;

use App\Http\Requests\FormRequest;

class StoreFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $personal = 'personal';
        $administrative = 'administrative';
        $idAssign = 'IdAssign';
        return [
            $personal.'.'.'first_name' => 'required|string|min:3|max:100',
            $personal.'.'.'last_name' => 'required|string|min:3|max:100',
            $personal.'.'.'email' => 'required|email|unique:employees,email',
            $personal.'.'.'phone' => 'required|max:50|unique:employees,phone',
            $personal.'.'.'dob' => 'required|date',
            $personal.'.'.'address' => 'required',
            $personal.'.'.'sub_district_id' => 'required|numeric',
            $personal.'.'.'nid' => 'required|max:17|unique:employees,nid',
            $personal.'.'.'zip_code' => 'required|string',
            $personal.'.'.'country' => 'required|string|max:100'
        ];
    }
}
