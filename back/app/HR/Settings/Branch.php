<?php

namespace App\HR\Settings;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
    		'name', 'slug', 'address', 'sub_district_id', 'postal_code', 'status_id'
    ];

    public function setNameAttribute($value)
    {
    	$this->attributes['name'] = $value;
    	$this->attributes['slug'] = str_slug( $value, $separator = '_');
    }

    public function administrative()
    {
        return $this->hasMany(HR\Employee\Administrative::class);
    }
}
